package pl.hemol.warmath.check

case class Exploding6(minRoll: Int) extends Exploding6Check {
  override def probabilityOfExact(k: Int, n: Int): BigDecimal = {
    (0 to k / 2).map({ explodes =>
      probabilityOfExplodes(explodes, n) * probabilityOfNonExplodes(minRoll, k - 2 * explodes, n - explodes)
    }).sum
  }

  override def explosionRatio: Int = 2
}
