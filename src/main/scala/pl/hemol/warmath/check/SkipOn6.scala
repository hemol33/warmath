package pl.hemol.warmath.check

case class SkipOn6(minRoll: Int, check: Check) extends D5Check {
  require(2 <= minRoll && minRoll <= 5)

  override def probabilityOfExact(k: Int, n: Int): BigDecimal = {
    (0 to k).map({ skips =>
      (k - skips to n - skips).map({ successes =>
        D5(minRoll).probabilityOfExact(successes, n - skips) * check.probabilityOfExact(k - skips, successes)
      }).sum * D6(6).probabilityOfExact(skips, n)
    }).sum
  }
}
