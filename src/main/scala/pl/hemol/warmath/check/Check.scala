package pl.hemol.warmath.check

import pl.hemol.warmath.Maths.{choose, pow}
import pl.hemol.warmath.Rolls


trait Check {
  // [0,1] probability of k successes in n tries
  def probabilityOfExact(k: Int, n: Int): BigDecimal

  // ratio of maximum successes to tries
  def explosionRatio: Int = 1

}

case class Gortach(minRoll: Int) extends Exploding6Check {
  override def probabilityOfExact(k: Int, n: Int): BigDecimal = {
    (0 to k).map({ explodeWounds =>
      (explodeWounds / 3 to explodeWounds).map({ explodes =>
        val quickProb = probabilityOfExplodes(explodes, n) * probabilityOfNonExplodes(minRoll, k - explodeWounds, n - explodes)
        if (quickProb == 0) {
          quickProb
        } else {
          quickProb * probabilityOfD3Addup(explodeWounds, explodes)
        }
      }).sum
    }).sum
  }

  private def probabilityOfD3Addup(wounds: Int, dice: Int): BigDecimal = {
    BigDecimal(Rolls.allPossibilities(dice, 3)
      .map(rolls => rolls.sum)
      .count(_ == wounds)) / BigDecimal(pow(3, dice))
  }

  override def explosionRatio: Int = 3
}


