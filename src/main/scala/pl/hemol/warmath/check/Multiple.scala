package pl.hemol.warmath.check

case class Multiple(checks: List[Check]) extends Check {
  override def probabilityOfExact(k: Int, n: Int): BigDecimal = {
    checks match {
      case check :: Nil => check.probabilityOfExact(k, n)
      case check :: other =>
        (0 to check.explosionRatio * n)
          .map(l => check.probabilityOfExact(l, n) * Multiple(other).probabilityOfExact(k, l))
          .sum
      case _ => throw new IllegalArgumentException("No check provided.")
    }
  }
}

object Multiple {
  def apply(checks: Check*): Multiple = Multiple(checks.toList)
}
