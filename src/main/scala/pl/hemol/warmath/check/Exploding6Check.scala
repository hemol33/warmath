package pl.hemol.warmath.check

import pl.hemol.warmath.Maths.{choose, pow}

trait Exploding6Check extends D6Check {
  protected def probabilityOfExplodes(k: Int, n: Int): BigDecimal = {
    D6(6).probabilityOfExact(k, n)
  }

  protected def probabilityOfNonExplodes(minRoll: Int, k: Int, n: Int): BigDecimal = {
    BigDecimal(exactNonExplodeCombinations(minRoll, k, n)) / BigDecimal(pow(5, n))
  }

  protected def exactNonExplodeCombinations(minRoll: Int, k: Int, n: Int): BigInt = {
    choose(n, k) * pow(success(minRoll) - 1, k) * pow(failure(minRoll), n - k)
  }
}
