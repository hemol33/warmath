package pl.hemol.warmath.check

case class Reroll(check: Check) extends Check {
  override def probabilityOfExact(k: Int, n: Int): BigDecimal = {
    (0 to k).map({ firstHits =>
      check.probabilityOfExact(firstHits, n) * check.probabilityOfExact(k - firstHits, n - firstHits)
    }).sum
  }
}
