package pl.hemol.warmath.check

import pl.hemol.warmath.Maths.{choose, pow}

case class D5(minRoll: Int) extends D5Check {
  require(1 <= minRoll && minRoll <= 5)

  override def probabilityOfExact(k: Int, n: Int): BigDecimal = {
    BigDecimal(exactCombinations(k, n)) / BigDecimal(pow(5, n))
  }

  private def exactCombinations(k: Int, n: Int): BigInt = {
    choose(n, k) * pow(success(minRoll), k) * pow(failure(minRoll), n - k)
  }
}
