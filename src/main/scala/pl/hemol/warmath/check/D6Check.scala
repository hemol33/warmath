package pl.hemol.warmath.check

trait D6Check extends Check {
  protected def success(score: Int): Int = 7 - score

  protected def failure(score: Int): Int = score - 1
}
