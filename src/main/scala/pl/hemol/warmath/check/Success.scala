package pl.hemol.warmath.check

case object Success extends Check {
  override def probabilityOfExact(k: Int, n: Int): BigDecimal = if (k == n) 1 else 0
}
