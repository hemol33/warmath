package pl.hemol.warmath.check

case class SkipOnRerolled6(minRoll: Int, check: Check) extends D5Check {
  require(2 <= minRoll && minRoll <= 5)

  override def probabilityOfExact(k: Int, n: Int): BigDecimal = {
    (0 to k).map({ skips1 =>
      (0 to k - skips1).map({ skips2 =>
        val skips = skips1 + skips2
        (k - skips to n - skips).map({ successes =>
          (0 to successes).map({ failsToSuccesses =>
            D5(minRoll).probabilityOfExact(successes - failsToSuccesses, n - skips1) *
              D5(minRoll).probabilityOfExact(failsToSuccesses, n - skips - successes + failsToSuccesses) *
              D6(6).probabilityOfExact(skips2, n - skips1 - successes + failsToSuccesses) *
              check.probabilityOfExact(k - skips, successes)
          }).sum
        }).sum * D6(6).probabilityOfExact(skips1, n)
      }).sum
    }).sum
  }
}
