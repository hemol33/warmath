package pl.hemol.warmath.check

case class Aim(minRoll: Int) extends Check {
  require(1 <= minRoll && minRoll <= 7)

  override def probabilityOfExact(k: Int, n: Int): BigDecimal = {
    if (minRoll == 7) {
      Multiple(D6(6), D6(4)).probabilityOfExact(k, n)
    } else {
      D6(minRoll).probabilityOfExact(k, n)
    }
  }
}
