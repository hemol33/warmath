package pl.hemol.warmath.check

import pl.hemol.warmath.Maths.{choose, pow}

case class D6(minRoll: Int) extends D6Check {
  require(1 <= minRoll && minRoll <= 6)

  override def probabilityOfExact(k: Int, n: Int): BigDecimal = {
    BigDecimal(exactCombinations(k, n, minRoll)) / BigDecimal(pow(6, n))
  }

  private def exactCombinations(k: Int, n: Int, score: Int): BigInt = {
    choose(n, k) * pow(success(score), k) * pow(failure(score), n - k)
  }
}
