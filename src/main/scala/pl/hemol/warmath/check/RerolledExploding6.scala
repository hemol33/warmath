package pl.hemol.warmath.check

case class RerolledExploding6(minRoll: Int) extends Exploding6Check {
  override def probabilityOfExact(k: Int, n: Int): BigDecimal = {
    (0 to k / 2).map({ explodes =>
      (0 to k - 2 * explodes).map({ onSeconds =>
        probabilityOfExplodes(explodes, n) * probabilityOfNonExplodes(minRoll, k - 2 * explodes - onSeconds, n - explodes) * D6(minRoll).probabilityOfExact(onSeconds, n)
      }).sum
    }).sum
  }

  override def explosionRatio: Int = 2
}
