package pl.hemol.warmath.check

import pl.hemol.warmath.Maths.{choose, pow}

case class CatapultHit(minRoll: Int) extends D6Check {
  require(1 <= minRoll && minRoll <= 6)

  override def probabilityOfExact(k: Int, n: Int): BigDecimal = {
    (0 to n - k).flatMap({ failures =>
      (0 to n - failures).map({ firstHits =>
        probabilityOfFailures(failures, n) * probabilityOfNonFailures(firstHits, failures, n) * D6(minRoll).probabilityOfExact(k - firstHits, n - firstHits - failures)
      })
    }).sum * atMostFailures(n - k, n)
  }

  private def probabilityOfFailures(f: Int, n: Int): BigDecimal = {
    D6(6).probabilityOfExact(f, n)
  }

  // P( exact(k, n) | f failures)
  private def probabilityOfNonFailures(k: Int, f: Int, n: Int): BigDecimal = {
    BigDecimal(exactNonFailureCombinations(k, n - f)) / BigDecimal(pow(5, n - f)) / BigDecimal(pow(6, f))
  }

  private def exactNonFailureCombinations(k: Int, n: Int): BigInt = {
    choose(n, k) * pow(success(minRoll), k) * pow(failure(minRoll) - 1, n - k)
  }

  private def atMostFailures(f: Int, n: Int): BigDecimal = {
    (0 to f).map(probabilityOfFailures(_, n)).sum
  }
}
