package pl.hemol.warmath.check

case class Reroll1(minRoll: Int) extends Check {
  require(2 <= minRoll && minRoll <= 6)

  override def probabilityOfExact(k: Int, n: Int): BigDecimal = {
    (0 to n).map({ rerolls =>
      (Math.max(0, k - rerolls) to k).map({ firstRounds =>
        D5(minRoll - 1).probabilityOfExact(firstRounds, n - rerolls) * D6(minRoll).probabilityOfExact(k - firstRounds, rerolls)
      }).sum * D6(6).probabilityOfExact(rerolls, n)
    }).sum
  }
}
