package pl.hemol.warmath

import scala.util.Random

object Rolls {
  def generate(n: Int, rng: Random): List[Int] = {
    (1 to n).map(_ => rng.nextInt(6) + 1).toList
  }

  def allPossibilities(n: Int, d: Int): Seq[List[Int]] = {
    def allPossibilitiesWithTail(k: Int, tail: List[Int]): Seq[List[Int]] = {
      if (k == 0) return Seq(tail)
      (1 to d).flatMap(value => allPossibilitiesWithTail(k - 1, value :: tail))
    }

    allPossibilitiesWithTail(n, Nil)
  }

  def main(args: Array[String]): Unit = {
    println(allPossibilities(5, 6).size)
  }
}
