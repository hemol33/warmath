package pl.hemol.warmath.fb9

import pl.hemol.warmath.Rolls

import scala.collection.parallel.CollectionConverters.ImmutableSeqIsParallelizable

object Spells {

  def main(args: Array[String]): Unit = {
    val results = (-2 to 2).par.map({ modifier =>
      (1 to 5).map({ cast =>
        (1 to 5).map({ dispel =>
          countCasts(cast, dispel, modifier)
        })
      })
    }).seq

    results.foreach({ modifier =>
      println("Modifier")
      modifier.foreach(row => {
        row.foreach(v => print(s"$v\t"));
        println
      })
    })
  }

  private def countCasts(cast: Int, dispel: Int, modifier: Int): Int = {

    def isCast(rolls: List[Int]): Boolean = {
      rolls.take(cast).sum + modifier > rolls.takeRight(dispel).sum
    }

    Rolls.allPossibilities(cast + dispel, 6)
      .count(isCast)
  }

}
