package pl.hemol.warmath.fb9

object Dispel {

  def main(args: Array[String]): Unit = {
    (3 to 32).foreach({ cast =>
      print(s"$cast\t")
      var rolls: Map[Int, BigInt] = Map(0 -> 1)
      (1 to 7).foreach({ dispels =>
        rolls = rolls.toSeq
          .flatMap({ case (sum, count) => (1 to 6).map(roll => (sum + roll) -> count) })
          .groupBy(_._1)
          .view.mapValues(_.map(_._2).sum)
          .toMap
        print(s"${rolls.filter(_._1 >= cast).values.sum}\t")
      })
      println
    })
  }

}
