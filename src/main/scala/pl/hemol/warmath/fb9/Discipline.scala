package pl.hemol.warmath.fb9

import pl.hemol.warmath.Rolls

object Discipline {

  def main(args: Array[String]): Unit = {
    Rolls.allPossibilities(2, 6)
      .map(_.sum)
      .groupBy(identity)
      .view.mapValues(_.size)
      .toSeq
      .sortBy(_._1)
      .foreach(pair => println(s"${pair._1}\t${pair._2}"))
  }

}
