package pl.hemol.warmath.fb9

import pl.hemol.warmath.Rolls

object Maximised {

  def main(args: Array[String]): Unit = {
    val max = 2
    val min = 0
    Rolls.allPossibilities(2 + max + min, 6)
      .map(eval(_, max, min))
      .groupBy(identity)
      .view.mapValues(_.size)
      .toSeq
      .sortBy(_._1)
      .foreach(pair => println(s"${pair._1}\t${pair._2}"))
  }

  private def eval(values: List[Int], max: Int, min: Int): Int = {
    values.sorted.drop(max).dropRight(min).sum
  }

}
