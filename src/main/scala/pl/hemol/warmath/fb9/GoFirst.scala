package pl.hemol.warmath.fb9

import pl.hemol.warmath.Rolls

object GoFirst {

  def main(args: Array[String]): Unit = {
    (1 to 5).foreach(l =>
      println(s"$l\t${
        Rolls.allPossibilities(2, 6)
          .count(rolls => rolls(0) + l > rolls(1))
      }")
    )
  }
}
