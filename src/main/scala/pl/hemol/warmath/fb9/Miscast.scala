package pl.hemol.warmath.fb9

import pl.hemol.warmath.Rolls

object Miscast {

  def main(args: Array[String]): Unit = {
    (3 to 5).foreach(n => {
      val validRolls = Rolls.allPossibilities(n, 6).count(isMiscast)
      println(s"$n\t$validRolls\t${validRolls / (math.pow(6, n))}")
    })
  }

  private def isMiscast(rolls: List[Int]): Boolean = {
    rolls.groupBy(identity).exists(_._2.size >= 3)
  }

}
