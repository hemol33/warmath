package pl.hemol.warmath

object Maths {

  def factorial(n: Int): BigInt = {
    (1 to n).map(BigInt(_)).product
  }

  def choose(n: Int, k: Int): BigInt = {
    if (k > n) return 0
    factorial(n) / factorial(k) / factorial(n - k)
  }

  def pow(n: Int, k: Int): BigInt = {
    if (k == 0) return 1

    val l: Int = k / 2
    val partial = pow(n, l)
    if (k == l * 2) {
      partial * partial
    } else {
      partial * partial * n
    }
  }

  def main(args: Array[String]): Unit = {
    println(
      s"""
         | 0! = ${factorial(0)}
         | 1! = ${factorial(1)}
         | 2! = ${factorial(2)}
         | 3! = ${factorial(3)}
         | 5! = ${factorial(5)}
         | 40! = ${factorial(40)}
         |
         | 1^0 = ${pow(1, 0)}
         | 1^2 = ${pow(1, 2)}
         | 2^0 = ${pow(2, 0)}
         | 2^8 = ${pow(2, 8)}
         | 5^2 = ${pow(5, 2)}
         |
         | (2 0) = ${choose(2, 0)}
         | (5 1) = ${choose(5, 1)}
         | (5 2) = ${choose(5, 2)}
         | (5 5) = ${choose(5, 5)}
         | (40, 20) = ${choose(40, 20)}
         |
         |""".stripMargin
    )
  }

}
