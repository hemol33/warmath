package pl.hemol.warmath

import pl.hemol.warmath.check._
import scalaz.Scalaz._

object Distributions {

  type Distribution = Map[Int, BigDecimal]

  private def getDistribution(check: Check, n: Int): Distribution = {
    (0 to n).map(l => l -> check.probabilityOfExact(l, n)).toMap
  }
  
  def median(distribution: Distribution): Int = {
    distribution.toSeq.sortBy(_._1).foldLeft((0, BigDecimal(0.0)))({ case ((m, cp), (k, p)) =>
      if (cp > 0.5) {
        (m, cp)
      } else
        (k, cp + p)
    })._1
  }

  def average(distributions: Distribution*): Distribution = {
    distributions.foldLeft(Map[Int, BigDecimal]())(_ |+| _)
      .map({ case (value, probability) => (value, probability / distributions.size) })
  }

  def sum(distribution1: Distribution, distribution2: Distribution): Distribution = {
    distribution1.toSeq.map({ case (value1, probability1) =>
      distribution2.map({ case (value2, probability2) =>
        (value1 + value2, probability1 * probability2)
      })
    }).reduce(_ |+| _)
  }

  def subtract(distribution1: Distribution, distribution2: Distribution): Distribution = {
    distribution1.toSeq.map({ case (value1, probability1) =>
      distribution2.map({ case (value2, probability2) =>
        (value1 - value2, probability1 * probability2)
      })
    }).reduce(_ |+| _)
  }

  def sumAll(distributions: Distribution*): Distribution = {
    distributions.reduce(sum)
  }

  def repeat(k: Int, distribution: Distribution): Distribution = {
    sumAll(List.fill(k)(distribution): _*)
  }

  def multiply(multiplier: Int, distribution: Distribution): Distribution = {
    distribution.map({ case (value, probability) => (multiplier * value, probability) })
  }

  def multipleWounds(distribution: Distribution, range: Seq[Int] = Seq(1, 2, 3)): Distribution = {
    average(range.map(multiplier =>
      multiply(multiplier, distribution)
    ): _*)
  }

  def cap(distribution: Distribution, cap: Int): Distribution = {
    val left = distribution.view.filterKeys(_ < cap).toMap
    val cappedProbability = distribution.view.filterKeys(_ >= cap).values.sum
    left + (cap -> cappedProbability)
  }

  def prettyPrint(distribution: Distribution): Unit = {
    distribution.toSeq
      .sortBy(_._1)
      .foreach({ case (value, probability) => println(s"$value\t${probability}") })

    println
    println(s"Median: ${median(distribution)}")
  }

  private def handOfHeaven(resilience: Int, armour: Int, check: Check = Success, upcast: Boolean = false): Distribution = {
    average(Rolls.allPossibilities(2, 6).map(rolls => {
      val up = if (upcast) 1 else 0
      val toWound = math.max(2, math.min(6, 4 - rolls.head - up + resilience))
      val save = math.max(1, math.min(6, 1 + armour - 2 - up))
      getDistribution(Multiple(D6(toWound), D6(save), check), rolls(1) + up)
    }): _*)
  }

}
