ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.12"

ThisBuild / organization := "pl.hemol"

lazy val root = (project in file("."))
  .settings(
    name := "warmath"
  )

libraryDependencies  ++= Seq(
  "org.scala-lang.modules" %% "scala-parallel-collections" % "1.0.4",
  "org.scalaz" %% "scalaz-core" % "7.3.7"
)
